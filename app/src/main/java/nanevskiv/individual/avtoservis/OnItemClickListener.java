package nanevskiv.individual.avtoservis;

public interface OnItemClickListener {
    void onItemClick(int position);
}
