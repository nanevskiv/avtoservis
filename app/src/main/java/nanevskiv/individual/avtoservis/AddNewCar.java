package nanevskiv.individual.avtoservis;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import nanevskiv.individual.avtoservis.databinding.ActivityAddNewCarBinding;

public class AddNewCar extends AppCompatActivity {

    private ActivityAddNewCarBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    private Common common = new Common();

    private ArrayAdapter<String> brandAdapter;
    private ArrayList<String> spinnerListBrand;
    private DatabaseReference brandReference;

    private ArrayAdapter<String> modelAdapter;
    private ArrayList<String> spinnerListModel;
    private DatabaseReference modelReference;

    private ArrayAdapter<String> yearAdapter;
    private ArrayList<String> spinnerListYear;

    @Override
    public void onStart() {
        super.onStart();
        common.hideProgressBar(binding.addCarProgressBar);
        populateBrands();
        populateYears();
    }

    // CLEARING SO BRANDS DON'T DUPLICATE
    @Override
    public void onPause() {
        super.onPause();
        brandAdapter.clear();
    }

    // PREVENT USER FROM GOING BACK AT THIS POINT
    @Override
    public void onBackPressed() {
        String message = "Додади автомобил за да продолжиш со регистрацијата";
        common.displaySnackbar(snackbarView, message, colorError, colorWhite);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddNewCarBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getTheme());

        snackbarView = findViewById(android.R.id.content);

        spinnerListBrand = new ArrayList<>();
        brandAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, spinnerListBrand);
        binding.addCarBrand.setAdapter(brandAdapter);

        spinnerListModel = new ArrayList<>();
        modelAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, spinnerListModel);
        binding.addCarModel.setAdapter(modelAdapter);

        spinnerListYear = new ArrayList<>();
        yearAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, spinnerListYear);
        binding.addCarYear.setAdapter(yearAdapter);

        binding.addCarSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String brand = binding.addCarBrand.getSelectedItem().toString().trim();
                String model = binding.addCarModel.getSelectedItem().toString().trim();
                String year = binding.addCarYear.getSelectedItem().toString().trim();

                AlertDialog.Builder dialog = new AlertDialog.Builder(AddNewCar.this);
                dialog.setMessage("Додади го " + brand + " " + model + " " + year
                        + " како твој автомобил?")
                        .setCancelable(true)
                        .setNegativeButton("НЕ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).setPositiveButton("ДА", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        common.showProgressBar(binding.addCarProgressBar);
                        addCar();
                    }
                });
                AlertDialog alertDialog = dialog.create();
                alertDialog.show();
            }
        });
    }

    private void populateBrands() {
        brandReference = FirebaseDatabase.getInstance().getReference().child("Car");

        brandReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    spinnerListBrand.add(item.getKey());
                }
                brandAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
        afterBrandSelected();
    }

    private void afterBrandSelected() {
        binding.addCarBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // CLEAR ADAPTER SO MODELS WONT DUPLICATE
                modelAdapter.clear();
                populateModels();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                binding.addCarBrand.setSelection(0);
            }
        });
    }

    private void populateModels() {
        modelReference = FirebaseDatabase.getInstance().getReference().child("Car")
                .child(binding.addCarBrand.getSelectedItem().toString());

        modelReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    spinnerListModel.add(item.getValue().toString());
                }
                modelAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }

    private void populateYears() {
        for (int i = 2020; i >= 1980; i--) {
            spinnerListYear.add(String.valueOf(i));
        }
        yearAdapter.notifyDataSetChanged();
    }

    private void addCar() {
        String brand = binding.addCarBrand.getSelectedItem().toString().trim();
        String model = binding.addCarModel.getSelectedItem().toString().trim();
        String year = binding.addCarYear.getSelectedItem().toString().trim();

        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("Cars");

        Car car = new Car(brand, model, year);
        String uploadID = mReference.push().getKey();

        mReference.child(uploadID).setValue(car)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        common.hideProgressBar(binding.addCarProgressBar);
                        startActivity(new Intent(AddNewCar.this, RegisterSuccess.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                common.displaySnackbar(snackbarView, e.getMessage(), colorError, colorWhite);
            }
        });
    }

}
