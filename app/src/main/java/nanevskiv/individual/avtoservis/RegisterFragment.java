package nanevskiv.individual.avtoservis;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nanevskiv.individual.avtoservis.databinding.FragmentRegisterBinding;

public class RegisterFragment extends Fragment {

    private FragmentRegisterBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    private Common common = new Common();

    @Override
    public void onStart() {
        super.onStart();
        common.hideProgressBar(binding.registerProgressBar);
    }

    public RegisterFragment() {
        // Required empty public constructor
    }

    private void disableInput() {
        binding.registerEmail.setEnabled(false);
        binding.registerPassword.setEnabled(false);
        binding.registerNameSurname.setEnabled(false);
        binding.registerPhoneNumber.setEnabled(false);
        binding.registerSubmit.setEnabled(false);
    }

    private void enableInput() {
        binding.registerEmail.setEnabled(true);
        binding.registerPassword.setEnabled(true);
        binding.registerNameSurname.setEnabled(true);
        binding.registerPhoneNumber.setEnabled(true);
        binding.registerSubmit.setEnabled(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getActivity().getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getActivity().getTheme());

        snackbarView = getActivity().findViewById(android.R.id.content);

        mAuth = FirebaseAuth.getInstance();
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer");

        binding.registerSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        return view;
    }

    private void validateInput() {
        String email = binding.registerEmail.getText().toString().trim();
        String password = binding.registerPassword.getText().toString().trim();
        String name = binding.registerNameSurname.getText().toString().trim();
        String phone = binding.registerPhoneNumber.getText().toString().trim();

        Pattern pattern = Pattern.compile("07[0-9]{7}");
        Matcher matcher = pattern.matcher(phone);

        if (email.isEmpty() || password.isEmpty() || name.isEmpty() || phone.isEmpty()) {
            String message = "Сите полиња се задолжителни";
            common.displaySnackbar(snackbarView, message, colorError, colorWhite);
        } else if (!matcher.matches()) {
            String message = "Телефонскиот број треба да е воформат: 07Х ХХХ ХХХ";
            common.displaySnackbar(snackbarView, message, colorError, colorWhite);
        } else {
            common.showProgressBar(binding.registerProgressBar);
            disableInput();
            registerCandidate();
        }
    }

    private void registerCandidate() {
        String email = binding.registerEmail.getText().toString().trim();
        String password = binding.registerPassword.getText().toString().trim();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            writeToDatabase();
                        } else {
                            common.hideProgressBar(binding.registerProgressBar);
                            common.displaySnackbar(snackbarView, task.getException().getMessage(), colorError, colorWhite);
                            enableInput();
                        }
                    }
                });
    }

    private void writeToDatabase() {
        String email = binding.registerEmail.getText().toString().trim();
        String name = binding.registerNameSurname.getText().toString().trim();
        String phone = binding.registerPhoneNumber.getText().toString().trim();
        int points = 100;

        Customer customer = new Customer(email, name, phone, points);
        String uploadID = common.usernameFromEmail(email);

        mReference.child(uploadID).setValue(customer)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        sendConfirmationEmail();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                common.hideProgressBar(binding.registerProgressBar);
                common.displaySnackbar(snackbarView, e.getMessage(), colorError, colorWhite);
                enableInput();
            }
        });
    }

    private void sendConfirmationEmail() {
        mAuth.getCurrentUser().sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            common.hideProgressBar(binding.registerProgressBar);
                            startActivity(new Intent(getActivity(), AddNewCar.class));
                        } else {
                            common.hideProgressBar(binding.registerProgressBar);
                            common.displaySnackbar(snackbarView,
                                    task.getException().getMessage(), colorError, colorWhite);
                            enableInput();
                        }
                    }
                });
    }
}
