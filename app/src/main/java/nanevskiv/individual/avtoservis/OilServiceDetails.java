package nanevskiv.individual.avtoservis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import nanevskiv.individual.avtoservis.databinding.ActivityOilServiceDetailsBinding;

import static nanevskiv.individual.avtoservis.FragmentOilService.EXTRA_CAR_OIL;
import static nanevskiv.individual.avtoservis.FragmentOilService.EXTRA_DATE_OIL;
import static nanevskiv.individual.avtoservis.FragmentOilService.EXTRA_DESC_OIL;
import static nanevskiv.individual.avtoservis.FragmentOilService.EXTRA_KM_OIL;

public class OilServiceDetails extends AppCompatActivity {

    private ActivityOilServiceDetailsBinding binding;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOilServiceDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        setSupportActionBar(binding.oilDetailsToolbar);
        getSupportActionBar().setTitle("");

        intent = getIntent();
        String date = intent.getStringExtra(EXTRA_DATE_OIL);
        String car = intent.getStringExtra(EXTRA_CAR_OIL);
        String description = intent.getStringExtra(EXTRA_DESC_OIL);
        String km = intent.getStringExtra(EXTRA_KM_OIL);

        binding.oilDetailsDate.setText(date);
        binding.oilDetailsCar.setText(car);
        binding.oilDetailsDescription.setText(description);
        binding.oilDetailsKm.setText(km);
    }
}
