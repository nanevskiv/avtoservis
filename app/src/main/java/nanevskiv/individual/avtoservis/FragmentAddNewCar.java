package nanevskiv.individual.avtoservis;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import nanevskiv.individual.avtoservis.databinding.FragmentAddNewCarBinding;

public class FragmentAddNewCar extends Fragment {

    private FragmentAddNewCarBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private int colorError;
    private int colorSuccess;
    private int colorWhite;
    private View snackbarView;
    private Common common = new Common();

    private ArrayAdapter<String> brandAdapter;
    private ArrayList<String> spinnerListBrand;
    private DatabaseReference brandReference;

    private ArrayAdapter<String> modelAdapter;
    private ArrayList<String> spinnerListModel;
    private DatabaseReference modelReference;

    private ArrayAdapter<String> yearAdapter;
    private ArrayList<String> spinnerListYear;

    @Override
    public void onStart() {
        super.onStart();
        common.hideProgressBar(binding.addNewCarProgressBar);
        populateBrands();
        populateYears();
    }

    // CLEARING SO BRANDS DON'T DUPLICATE
    @Override
    public void onPause() {
        super.onPause();
        brandAdapter.clear();
    }

    public FragmentAddNewCar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAddNewCarBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getActivity().getTheme());
        colorSuccess = getResources().getColor(R.color.colorSuccess, getActivity().getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getActivity().getTheme());

        snackbarView = getActivity().findViewById(android.R.id.content);

        spinnerListBrand = new ArrayList<>();
        brandAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item, spinnerListBrand);
        binding.addNewCarBrand.setAdapter(brandAdapter);

        spinnerListModel = new ArrayList<>();
        modelAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item, spinnerListModel);
        binding.addNewCarModel.setAdapter(modelAdapter);

        spinnerListYear = new ArrayList<>();
        yearAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item, spinnerListYear);
        binding.addNewCarYear.setAdapter(yearAdapter);

        binding.addNewCarSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String brand = binding.addNewCarBrand.getSelectedItem().toString().trim();
                String model = binding.addNewCarModel.getSelectedItem().toString().trim();
                String year = binding.addNewCarYear.getSelectedItem().toString().trim();

                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setMessage("Додади го " + brand + " " + model + " " + year
                        + " како твој автомобил?")
                        .setCancelable(true)
                        .setNegativeButton("НЕ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).setPositiveButton("ДА", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        common.showProgressBar(binding.addNewCarProgressBar);
                        addCar();
                    }
                });
                AlertDialog alertDialog = dialog.create();
                alertDialog.show();
            }
        });

        return view;
    }

    private void populateBrands() {
        brandReference = FirebaseDatabase.getInstance().getReference().child("Car");

        brandReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    spinnerListBrand.add(item.getKey());
                }
                brandAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
        afterBrandSelected();
    }

    private void afterBrandSelected() {
        binding.addNewCarBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // CLEAR ADAPTER SO MODELS WONT DUPLICATE
                modelAdapter.clear();
                populateModels();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                binding.addNewCarBrand.setSelection(0);
            }
        });
    }

    private void populateModels() {
        modelReference = FirebaseDatabase.getInstance().getReference().child("Car")
                .child(binding.addNewCarBrand.getSelectedItem().toString());

        modelReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    spinnerListModel.add(item.getValue().toString());
                }
                modelAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }

    private void populateYears() {
        for (int i = 2020; i >= 1980; i--) {
            spinnerListYear.add(String.valueOf(i));
        }
        yearAdapter.notifyDataSetChanged();
    }

    private void addCar() {
        String brand = binding.addNewCarBrand.getSelectedItem().toString().trim();
        String model = binding.addNewCarModel.getSelectedItem().toString().trim();
        String year = binding.addNewCarYear.getSelectedItem().toString().trim();

        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("Cars");

        Car car = new Car(brand, model, year);
        String uploadID = mReference.push().getKey();

        mReference.child(uploadID).setValue(car)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        common.hideProgressBar(binding.addNewCarProgressBar);
                        String message = "Успешно додаден автомобил";
                        common.displaySnackbar(snackbarView, message, colorSuccess, colorWhite);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                common.displaySnackbar(snackbarView, e.getMessage(), colorError, colorWhite);
            }
        });
    }
}
