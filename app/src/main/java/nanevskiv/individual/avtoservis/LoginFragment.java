package nanevskiv.individual.avtoservis;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import nanevskiv.individual.avtoservis.databinding.FragmentLoginBinding;

public class LoginFragment extends Fragment {

    private FragmentLoginBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    private Common common = new Common();

    @Override
    public void onStart() {
        super.onStart();
        common.hideProgressBar(binding.loginProgressBar);
    }

    private void disableInput() {
        binding.loginEmail.setEnabled(false);
        binding.loginPassword.setEnabled(false);
        binding.loginSubmit.setEnabled(false);
    }

    private void enableInput() {
        binding.loginEmail.setEnabled(true);
        binding.loginPassword.setEnabled(true);
        binding.loginSubmit.setEnabled(true);
    }

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getActivity().getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getActivity().getTheme());

        snackbarView = getActivity().findViewById(android.R.id.content);

        mAuth = FirebaseAuth.getInstance();

        binding.loginSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        binding.loginForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ForgotPassword.class));
            }
        });

        return view;
    }

    private void validateInput() {
        String email = binding.loginEmail.getText().toString().trim();
        String password = binding.loginPassword.getText().toString().trim();

        if (email.isEmpty() || password.isEmpty()) {
            String message = "Сите полиња се задолжителни";
            common.displaySnackbar(snackbarView, message, colorError, colorWhite);
        } else {
            common.showProgressBar(binding.loginProgressBar);
            disableInput();
            checkIfAllowed();
        }
    }

    private void checkIfAllowed() {
        String email = binding.loginEmail.getText().toString().trim();
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(common.usernameFromEmail(email));

        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    String message = "E-mail адресата не е асоцирана со кориснички профил";
                    common.hideProgressBar(binding.loginProgressBar);
                    common.displaySnackbar(snackbarView, message, colorError, colorWhite);
                    enableInput();
                } else {
                    loginCustomer();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.hideProgressBar(binding.loginProgressBar);
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
                enableInput();
            }
        });
    }

    private void loginCustomer() {
        String email = binding.loginEmail.getText().toString().trim();
        String password = binding.loginPassword.getText().toString().trim();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            if (!mAuth.getCurrentUser().isEmailVerified()) {
                                common.hideProgressBar(binding.loginProgressBar);
                                String message = "Непотврдена e-mail адреса";
                                common.displaySnackbar(snackbarView, message, colorError, colorWhite);
                                enableInput();
                            } else {
                                common.hideProgressBar(binding.loginProgressBar);
                                startActivity(new Intent(getActivity(), CustomerHome.class));
                            }
                        } else {
                            common.hideProgressBar(binding.loginProgressBar);
                            common.displaySnackbar(snackbarView, task.getException()
                                    .getMessage(), colorError, colorWhite);
                            enableInput();
                        }
                    }
                });
    }
}
