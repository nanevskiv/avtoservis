package nanevskiv.individual.avtoservis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import nanevskiv.individual.avtoservis.databinding.ActivitySmallServiceDetailsBinding;

import static nanevskiv.individual.avtoservis.FragmentSmallService.EXTRA_CAR_SMALL;
import static nanevskiv.individual.avtoservis.FragmentSmallService.EXTRA_DATE_SMALL;
import static nanevskiv.individual.avtoservis.FragmentSmallService.EXTRA_DESC_SMALL;
import static nanevskiv.individual.avtoservis.FragmentSmallService.EXTRA_KM_SMALL;

public class SmallServiceDetails extends AppCompatActivity {

    private ActivitySmallServiceDetailsBinding binding;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySmallServiceDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        setSupportActionBar(binding.smallDetailsToolbar);
        getSupportActionBar().setTitle("");

        intent = getIntent();
        String date = intent.getStringExtra(EXTRA_DATE_SMALL);
        String car = intent.getStringExtra(EXTRA_CAR_SMALL);
        String description = intent.getStringExtra(EXTRA_DESC_SMALL);
        String km = intent.getStringExtra(EXTRA_KM_SMALL);

        binding.smallDetailsDate.setText(date);
        binding.smallDetailsCar.setText(car);
        binding.smallDetailsDescription.setText(description);
        binding.smallDetailsKm.setText(km);
    }
}
