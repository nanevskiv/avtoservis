package nanevskiv.individual.avtoservis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BigServiceAdapter extends RecyclerView.Adapter<BigServiceAdapter.BigServiceViewHolder> {

    private Context context;
    private List<BigService> bigServiceList;
    private OnItemClickListener mListener;

    void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    BigServiceAdapter(Context context, List<BigService> bigServiceList) {
        this.context = context;
        this.bigServiceList = bigServiceList;
    }

    @NonNull
    @Override
    public BigServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.service_item,
                parent, false);
        return new BigServiceAdapter.BigServiceViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull BigServiceViewHolder holder, int position) {
        BigService bigService = bigServiceList.get(position);
        holder.serviceDate.setText(bigService.getServiceDate());
    }

    @Override
    public int getItemCount() {
        return bigServiceList.size();
    }


    static class BigServiceViewHolder extends RecyclerView.ViewHolder {

        TextView serviceDate;

        BigServiceViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            serviceDate = itemView.findViewById(R.id.service_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

}

