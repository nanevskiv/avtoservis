package nanevskiv.individual.avtoservis;

public class MainService {

    private String serviceCar;
    private String serviceKM;
    private String serviceDescription;
    private String serviceDate;

    public MainService(){}

    public MainService(String serviceCar, String serviceKM, String serviceDescription, String serviceDate) {
        this.serviceCar = serviceCar;
        this.serviceKM = serviceKM;
        this.serviceDescription = serviceDescription;
        this.serviceDate = serviceDate;
    }

    public String getServiceCar() {
        return serviceCar;
    }

    public String getServiceKM() {
        return serviceKM;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public String getServiceDate() {
        return serviceDate;
    }
}
