package nanevskiv.individual.avtoservis;

public class Promotion {

    private String promotionTitle;
    private String promotionContent;
    private String promotionImage;

    public Promotion() {
    }

    public Promotion(String promotionTitle, String promotionContent, String promotionImage) {
        this.promotionTitle = promotionTitle;
        this.promotionContent = promotionContent;
        this.promotionImage = promotionImage;
    }

    public String getPromotionTitle() {
        return promotionTitle;
    }

    public String getPromotionContent() {
        return promotionContent;
    }

    public String getPromotionImage() {
        return promotionImage;
    }

}
