package nanevskiv.individual.avtoservis;

public class Car {

    private String carBrand;
    private String carModel;
    private String carYear;

    public Car(String carBrand, String carModel, String carYear) {
        this.carBrand = carBrand;
        this.carModel = carModel;
        this.carYear = carYear;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarYear() {
        return carYear;
    }
}
