package nanevskiv.individual.avtoservis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.squareup.picasso.Picasso;

import nanevskiv.individual.avtoservis.databinding.ActivityPromotionDetailsBinding;

import static nanevskiv.individual.avtoservis.FragmentPromotion.EXTRA_CONTENT;
import static nanevskiv.individual.avtoservis.FragmentPromotion.EXTRA_IMAGE;
import static nanevskiv.individual.avtoservis.FragmentPromotion.EXTRA_TITLE;

public class PromotionDetails extends AppCompatActivity {

    private ActivityPromotionDetailsBinding binding;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPromotionDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        setSupportActionBar(binding.promotionDetailsToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();
        String image = intent.getStringExtra(EXTRA_IMAGE);
        String title = intent.getStringExtra(EXTRA_TITLE);
        String content = intent.getStringExtra(EXTRA_CONTENT);

        Picasso.get()
                .load(image)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(binding.promotionDetailsImage);
        binding.promotionDetailsTitle.setText(title);
        binding.promotionDetailsContent.setText(content);
    }
}
