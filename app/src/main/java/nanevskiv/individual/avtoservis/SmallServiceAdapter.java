package nanevskiv.individual.avtoservis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SmallServiceAdapter extends RecyclerView.Adapter<SmallServiceAdapter.SmallServiceViewHolder> {

    private Context context;
    private List<SmallService> smallServiceList;
    private OnItemClickListener mListener;

    void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    SmallServiceAdapter(Context context, List<SmallService> smallServiceList) {
        this.context = context;
        this.smallServiceList = smallServiceList;
    }

    @NonNull
    @Override
    public SmallServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.service_item,
                parent, false);
        return new SmallServiceAdapter.SmallServiceViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SmallServiceViewHolder holder, int position) {
        SmallService smallService = smallServiceList.get(position);
        holder.serviceDate.setText(smallService.getServiceDate());
    }

    @Override
    public int getItemCount() {
        return smallServiceList.size();
    }


    static class SmallServiceViewHolder extends RecyclerView.ViewHolder {

        TextView serviceDate;

        SmallServiceViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            serviceDate = itemView.findViewById(R.id.service_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
