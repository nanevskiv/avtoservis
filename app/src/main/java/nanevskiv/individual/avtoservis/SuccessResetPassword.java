package nanevskiv.individual.avtoservis;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import nanevskiv.individual.avtoservis.databinding.ActivitySuccessResetPasswordBinding;

public class SuccessResetPassword extends AppCompatActivity {

    private ActivitySuccessResetPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySuccessResetPasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


    }
}
