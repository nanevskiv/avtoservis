package nanevskiv.individual.avtoservis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class OilServiceAdapter extends RecyclerView.Adapter<OilServiceAdapter.OilServiceViewHolder> {

    private Context context;
    private List<OilService> oilServiceList;
    private OnItemClickListener mListener;

    void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    OilServiceAdapter(Context context, List<OilService> oilServiceList) {
        this.context = context;
        this.oilServiceList = oilServiceList;
    }

    @NonNull
    @Override
    public OilServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.service_item,
                parent, false);
        return new OilServiceAdapter.OilServiceViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull OilServiceViewHolder holder, int position) {
        OilService oilService = oilServiceList.get(position);
        holder.serviceDate.setText(oilService.getServiceDate());
    }

    @Override
    public int getItemCount() {
        return oilServiceList.size();
    }

    static class OilServiceViewHolder extends RecyclerView.ViewHolder {
        TextView serviceDate;

        OilServiceViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            serviceDate = itemView.findViewById(R.id.service_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

}
