package nanevskiv.individual.avtoservis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import nanevskiv.individual.avtoservis.databinding.ActivityBigServiceDetailsBinding;

import static nanevskiv.individual.avtoservis.FragmentBigService.EXTRA_CAR_BIG;
import static nanevskiv.individual.avtoservis.FragmentBigService.EXTRA_DATE_BIG;
import static nanevskiv.individual.avtoservis.FragmentBigService.EXTRA_DESC_BIG;
import static nanevskiv.individual.avtoservis.FragmentBigService.EXTRA_KM_BIG;

public class BigServiceDetails extends AppCompatActivity {

    private ActivityBigServiceDetailsBinding binding;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBigServiceDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        setSupportActionBar(binding.bigDetailsToolbar);
        getSupportActionBar().setTitle("");

        intent = getIntent();
        String date = intent.getStringExtra(EXTRA_DATE_BIG);
        String car = intent.getStringExtra(EXTRA_CAR_BIG);
        String description = intent.getStringExtra(EXTRA_DESC_BIG);
        String km = intent.getStringExtra(EXTRA_KM_BIG);

        binding.bigDetailsDate.setText(date);
        binding.bigDetailsCar.setText(car);
        binding.bigDetailsDescription.setText(description);
        binding.bigDetailsKm.setText(km);
    }
}
