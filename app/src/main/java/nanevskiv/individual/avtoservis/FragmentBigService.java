package nanevskiv.individual.avtoservis;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nanevskiv.individual.avtoservis.databinding.FragmentBigServiceBinding;

import static java.time.temporal.ChronoUnit.DAYS;

public class FragmentBigService extends Fragment {

    private FragmentBigServiceBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private List<BigService> bigServiceList;
    private BigServiceAdapter bigServiceAdapter;
    private DatabaseReference carReference;
    private ArrayAdapter<String> carAdapter;
    private ArrayList<String> carList;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    static final String EXTRA_DATE_BIG = "bigDate";
    static final String EXTRA_CAR_BIG = "bigCar";
    static final String EXTRA_DESC_BIG = "bigDesc";
    static final String EXTRA_KM_BIG = "bigKM";
    private Common common = new Common();

    @Override
    public void onStart() {
        super.onStart();
        populateCars();
    }

    @Override
    public void onPause() {
        super.onPause();
        carAdapter.clear();
    }

    public FragmentBigService() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentBigServiceBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.bigServiceToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getActivity().getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getActivity().getTheme());

        snackbarView = getActivity().findViewById(android.R.id.content);

        carList = new ArrayList<>();
        carAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item, carList);
        binding.bigServiceCar.setAdapter(carAdapter);
        binding.bigServiceCar.setSelection(0);

        binding.bigServiceRecyclerView.setHasFixedSize(true);
        binding.bigServiceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        bigServiceList = new ArrayList<>();

        binding.bigServiceCar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                populateBigServices();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                binding.bigServiceCar.setSelection(0);
            }
        });

        mAuth = FirebaseAuth.getInstance();
        binding.bigServiceSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                common.signOutPressed(mAuth, getActivity());
            }
        });

        return view;
    }

    private void populateCars() {
        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        carReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("Cars");

        carReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // CLEAR SO ITEMS DON'T DUPLICATE
                carList.clear();

                for (DataSnapshot item : snapshot.getChildren()) {
                    carList.add(item.child("carBrand").getValue().toString() + " "
                            + item.child("carModel").getValue().toString() + " "
                            + item.child("carYear").getValue().toString());
                }
                carAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }

    private void populateBigServices() {
        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("bigService");

        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // CLEAR SO ITEMS DON'T DUPLICATE
                bigServiceList.clear();

                for (DataSnapshot item : snapshot.getChildren()) {
                    BigService bigService = item.getValue(BigService.class);

                    // ONLY SHOW SERVICES FOR CURRENTLY SELECTED CAR
                    if (bigService.getServiceCar().equals(binding.bigServiceCar.getSelectedItem().toString())) {
                        bigServiceList.add(bigService);

                        binding.bigServiceImage.setVisibility(View.GONE);
                        binding.bigServiceRecyclerView.setVisibility(View.VISIBLE);
                        binding.bigServiceDivider.setVisibility(View.VISIBLE);
                        binding.bigServiceCard.setVisibility(View.VISIBLE);

                        // GET DATE POSTED FROM DB
                        LocalDate datePosted = LocalDate.parse(item.child("servicePosted")
                                .getValue().toString());
                        // ADD 3 YEARS TO POSTED DATE AS BIG SERVICE
                        // SHOULD BE DONE EVERY 3 YEARс
                        LocalDate dateFuture = datePosted.plusYears(3);
                        LocalDate now = LocalDate.now();
                        // GET DAYS BETWEEN CURRENT DATE AND DATE WHEN
                        // NEXT OIL SERVICE SHOULD BE DONE
                        long noOfDaysBetween = DAYS.between(now, dateFuture);
                        String result = String.valueOf(noOfDaysBetween);

                        if (result.charAt(result.length() - 1) == '1')
                            binding.bigServiceRemainingTime.setText(getString
                                    (R.string.remaining_time_big_one, result));
                        else
                            binding.bigServiceRemainingTime.setText(getString
                                    (R.string.remaining_time_big, result));
                    }

                    if (bigServiceList.isEmpty()) {
                        binding.bigServiceImage.setVisibility(View.VISIBLE);
                        binding.bigServiceRecyclerView.setVisibility(View.GONE);
                        binding.bigServiceDivider.setVisibility(View.GONE);
                        binding.bigServiceCard.setVisibility(View.GONE);
                    }

                    bigServiceAdapter = new BigServiceAdapter(getActivity(), bigServiceList);
                    binding.bigServiceRecyclerView.setAdapter(bigServiceAdapter);
                }
                if (bigServiceList.size() > 0) {
                    Collections.reverse(bigServiceList);
                    bigServiceAdapter.notifyDataSetChanged();

                    bigServiceAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            Intent intent = new Intent(getActivity(), BigServiceDetails.class);
                            BigService clickedBigService = bigServiceList.get(position);
                            intent.putExtra(EXTRA_DATE_BIG, clickedBigService.getServiceDate());
                            intent.putExtra(EXTRA_CAR_BIG, clickedBigService.getServiceCar());
                            intent.putExtra(EXTRA_DESC_BIG, clickedBigService.getServiceDescription());
                            intent.putExtra(EXTRA_KM_BIG, clickedBigService.getServiceKM());
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }
}
