package nanevskiv.individual.avtoservis;

public class BigService extends MainService {

    private String servicePosted;

    public BigService() {
    }

    public BigService(String serviceCar, String serviceKM, String serviceDescription,
                      String serviceDate, String servicePosted) {
        super(serviceCar, serviceKM, serviceDescription, serviceDate);
        this.servicePosted = servicePosted;
    }

    public String getServicePosted() {
        return servicePosted;
    }
}
