package nanevskiv.individual.avtoservis;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nanevskiv.individual.avtoservis.databinding.FragmentPromotionBinding;

public class FragmentPromotion extends Fragment {

    private FragmentPromotionBinding binding;
    private DatabaseReference mReference;
    private FirebaseAuth mAuth;
    private List<Promotion> promotionList;
    private PromotionAdapter promotionAdapter;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    static final String EXTRA_IMAGE = "promotionImage";
    static final String EXTRA_TITLE = "promotionTitle";
    static final String EXTRA_CONTENT = "promotionContent";
    private Common common = new Common();

    public FragmentPromotion() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentPromotionBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.promotionToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getActivity().getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getActivity().getTheme());

        snackbarView = getActivity().findViewById(android.R.id.content);

        mAuth = FirebaseAuth.getInstance();
        binding.promotionSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                common.signOutPressed(mAuth, getActivity());
            }
        });

        binding.promotionPointsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        binding.promotionPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        getPoints();
        populatePromotions();

        return view;
    }

    private void showDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(R.string.points_explanation)
                .setCancelable(true)
                .setNegativeButton("Во Ред", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }


    private void getPoints() {
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer").child(username);

        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String points = snapshot.child("customerPoints").getValue().toString();
                binding.promotionPoints.setText(points);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }

    private void populatePromotions() {
        binding.promotionRecyclerView.setHasFixedSize(true);
        binding.promotionRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        promotionList = new ArrayList<>();
        mReference = FirebaseDatabase.getInstance().getReference().child("Promotion");

        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // CLEAR HERE SO ITEMS DON'T DUPLICATE
                promotionList.clear();

                for (DataSnapshot item : snapshot.getChildren()) {
                    Promotion promotion = item.getValue(Promotion.class);
                    promotionList.add(promotion);

                    promotionAdapter = new PromotionAdapter(getActivity(), promotionList);
                    binding.promotionRecyclerView.setAdapter(promotionAdapter);
                }
                if (promotionList.size() > 0) {
                    Collections.reverse(promotionList);
                    promotionAdapter.notifyDataSetChanged();

                    promotionAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            Intent intent = new Intent(getActivity(), PromotionDetails.class);
                            Promotion clickedPromotion = promotionList.get(position);

                            intent.putExtra(EXTRA_IMAGE, clickedPromotion.getPromotionImage());
                            intent.putExtra(EXTRA_TITLE, clickedPromotion.getPromotionTitle());
                            intent.putExtra(EXTRA_CONTENT, clickedPromotion.getPromotionContent());
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }
}
