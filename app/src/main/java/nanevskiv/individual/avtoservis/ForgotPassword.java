package nanevskiv.individual.avtoservis;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import nanevskiv.individual.avtoservis.databinding.ActivityForgotPasswordBinding;

public class ForgotPassword extends AppCompatActivity {

    private ActivityForgotPasswordBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    private Common common = new Common();

    @Override
    protected void onStart() {
        super.onStart();
        common.hideProgressBar(binding.forgotPasswordProgressBar);
    }

    private void disableInput() {
        binding.forgotPasswordEmail.setEnabled(false);
        binding.forgotPasswordSubmit.setEnabled(false);
    }

    private void enableInput() {
        binding.forgotPasswordEmail.setEnabled(true);
        binding.forgotPasswordSubmit.setEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityForgotPasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        //  COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getTheme());

        snackbarView = findViewById(android.R.id.content);

        mAuth = FirebaseAuth.getInstance();

        binding.forgotPasswordSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });
    }

    private void validateInput() {
        String email = binding.forgotPasswordEmail.getText().toString().trim();

        if (email.isEmpty()) {
            String message = "Сите полиња се задолжителни";
            common.displaySnackbar(snackbarView, message, colorError, colorWhite);
        } else {
            common.showProgressBar(binding.forgotPasswordProgressBar);
            disableInput();
            checkIfAllowed();
        }
    }

    private void checkIfAllowed() {
        String email = binding.forgotPasswordEmail.getText().toString().trim();
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(common.usernameFromEmail(email));

        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    String message = "E-mail адресата не е асоцирана со кориснички профил";
                    common.hideProgressBar(binding.forgotPasswordProgressBar);
                    common.displaySnackbar(snackbarView, message, colorError, colorWhite);
                    enableInput();
                } else {
                    sendResetEmail();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.hideProgressBar(binding.forgotPasswordProgressBar);
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
                enableInput();
            }
        });
    }

    private void sendResetEmail() {
        String email = binding.forgotPasswordEmail.getText().toString().trim();

        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            common.hideProgressBar(binding.forgotPasswordProgressBar);
                            startActivity(new Intent(ForgotPassword.this,
                                    SuccessResetPassword.class));
                            finish();
                        } else {
                            common.hideProgressBar(binding.forgotPasswordProgressBar);
                            common.displaySnackbar(snackbarView, task.getException().getMessage(),
                                    colorError, colorWhite);
                            enableInput();
                        }
                    }
                });
    }
}
