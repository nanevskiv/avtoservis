package nanevskiv.individual.avtoservis;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

class Common {

    void hideProgressBar(ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
    }

    void showProgressBar(ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
    }

    // REPLACING '.' TO 't' BECAUSE FIREBASE CANNOT SAVE DATA THAT HAS '.' IN IT
    String usernameFromEmail(String email) {
        return email.replace('.', 't');
    }

    void displaySnackbar(View view, String message, int bgColor, int textColor) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setBackgroundTint(bgColor);
        snackbar.setTextColor(textColor);
        snackbar.show();
    }

    void signOutPressed(final FirebaseAuth mAuth, final Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage("Одјави се?")
                .setCancelable(true)
                .setNegativeButton("НЕ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAuth.signOut();
                context.startActivity(new Intent(context, MainActivity.class));
            }
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

}
