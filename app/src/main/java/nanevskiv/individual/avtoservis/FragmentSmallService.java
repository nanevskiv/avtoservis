package nanevskiv.individual.avtoservis;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nanevskiv.individual.avtoservis.databinding.FragmentSmallServiceBinding;


public class FragmentSmallService extends Fragment {

    private FragmentSmallServiceBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private List<SmallService> smallServiceList;
    private SmallServiceAdapter smallServiceAdapter;
    private DatabaseReference carReference;
    private ArrayAdapter<String> carAdapter;
    private ArrayList<String> carList;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    static final String EXTRA_DATE_SMALL = "smallDate";
    static final String EXTRA_CAR_SMALL = "smallCar";
    static final String EXTRA_DESC_SMALL = "smallDesc";
    static final String EXTRA_KM_SMALL = "smallKM";
    private Common common = new Common();

    @Override
    public void onStart() {
        super.onStart();
        populateCars();
    }

    @Override
    public void onPause() {
        super.onPause();
        carAdapter.clear();
    }

    public FragmentSmallService() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentSmallServiceBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.smallServiceToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getActivity().getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getActivity().getTheme());

        snackbarView = getActivity().findViewById(android.R.id.content);

        carList = new ArrayList<>();
        carAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item, carList);
        binding.smallServiceCar.setAdapter(carAdapter);
        binding.smallServiceCar.setSelection(0);

        binding.smallServiceCar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                populateSmallServices();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                binding.smallServiceCar.setSelection(0);
            }
        });

        binding.smallServiceRecyclerView.setHasFixedSize(true);
        binding.smallServiceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        smallServiceList = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        binding.smallServiceSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                common.signOutPressed(mAuth, getActivity());
            }
        });

        return view;
    }

    private void populateCars() {
        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        carReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("Cars");

        carReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // CLEAR SO ITEMS DON'T DUPLICATE
                carList.clear();

                for (DataSnapshot item : snapshot.getChildren()) {
                    carList.add(item.child("carBrand").getValue().toString() + " "
                            + item.child("carModel").getValue().toString() + " "
                            + item.child("carYear").getValue().toString());
                }
                carAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }

    private void populateSmallServices() {
        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("smallService");

        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // CLEARING HERE SO DATA WONT DUPLICATE
                smallServiceList.clear();

                for (DataSnapshot item : snapshot.getChildren()) {
                    SmallService smallService = item.getValue(SmallService.class);

                    // ONLY SHOW SERVICES FOR CURRENTLY SELECTED CAR
                    if (smallService.getServiceCar().equals(binding.smallServiceCar
                            .getSelectedItem().toString())) {
                        smallServiceList.add(smallService);

                        binding.smallServiceImage.setVisibility(View.GONE);
                        binding.smallServiceRecyclerView.setVisibility(View.VISIBLE);
                    }

                    if (smallServiceList.isEmpty()) {
                        binding.smallServiceImage.setVisibility(View.VISIBLE);
                        binding.smallServiceRecyclerView.setVisibility(View.GONE);
                    }

                    smallServiceAdapter = new SmallServiceAdapter(getActivity(), smallServiceList);
                    binding.smallServiceRecyclerView.setAdapter(smallServiceAdapter);
                }
                if (smallServiceList.size() > 0) {
                    Collections.reverse(smallServiceList);
                    smallServiceAdapter.notifyDataSetChanged();

                    smallServiceAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            Intent intent = new Intent(getActivity(), SmallServiceDetails.class);
                            SmallService clickedSmallService = smallServiceList.get(position);
                            intent.putExtra(EXTRA_DATE_SMALL, clickedSmallService.getServiceDate());
                            intent.putExtra(EXTRA_CAR_SMALL, clickedSmallService.getServiceCar());
                            intent.putExtra(EXTRA_DESC_SMALL, clickedSmallService.getServiceDescription());
                            intent.putExtra(EXTRA_KM_SMALL, clickedSmallService.getServiceKM());
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }
}
