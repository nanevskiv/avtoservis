package nanevskiv.individual.avtoservis;

public class Customer {

    private String customerEmail;
    private String customerNameSurname;
    private String customerPhone;
    private int customerPoints;

    public Customer() {
    }

    public Customer(String customerEmail, String customerNameSurname, String customerPhone,
                    int customerPoints) {
        this.customerEmail = customerEmail;
        this.customerNameSurname = customerNameSurname;
        this.customerPhone = customerPhone;
        this.customerPoints = customerPoints;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public String getCustomerNameSurname() {
        return customerNameSurname;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public int getCustomerPoints() {
        return customerPoints;
    }
}
