package nanevskiv.individual.avtoservis;

public class OilService extends MainService {

    private String servicePosted;

    public OilService() {
    }

    public OilService(String serviceCar, String serviceKM, String serviceDescription,
                      String serviceDate, String servicePosted) {
        super(serviceCar, serviceKM, serviceDescription, serviceDate);
        this.servicePosted = servicePosted;
    }

    public String getServicePosted() {
        return servicePosted;
    }

}
