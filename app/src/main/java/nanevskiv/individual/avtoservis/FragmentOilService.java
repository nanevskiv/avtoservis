package nanevskiv.individual.avtoservis;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nanevskiv.individual.avtoservis.databinding.FragmentOilServiceBinding;

import static java.time.temporal.ChronoUnit.DAYS;

public class FragmentOilService extends Fragment {

    private FragmentOilServiceBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mReference;
    private List<OilService> oilServiceList;
    private OilServiceAdapter oilServiceAdapter;
    private DatabaseReference carReference;
    private ArrayAdapter<String> carAdapter;
    private ArrayList<String> carList;
    private int colorError;
    private int colorWhite;
    private View snackbarView;
    static final String EXTRA_KM_OIL = "oilKM";
    static final String EXTRA_DESC_OIL = "oilDesc";
    static final String EXTRA_DATE_OIL = "oilDate";
    static final String EXTRA_CAR_OIL = "oilCar";
    private Common common = new Common();

    @Override
    public void onStart() {
        super.onStart();
        populateCars();
    }

    @Override
    public void onPause() {
        super.onPause();
        carAdapter.clear();
    }

    public FragmentOilService() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentOilServiceBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.oilServiceToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        // COLORS FOR DISPLAYING SNACKBAR
        colorError = getResources().getColor(R.color.colorError, getActivity().getTheme());
        colorWhite = getResources().getColor(R.color.colorWhite, getActivity().getTheme());

        snackbarView = getActivity().findViewById(android.R.id.content);

        carList = new ArrayList<>();
        carAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item, carList);
        binding.oilServiceCar.setAdapter(carAdapter);
        binding.oilServiceCar.setSelection(0);

        binding.oilServiceRecyclerView.setHasFixedSize(true);
        binding.oilServiceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        oilServiceList = new ArrayList<>();

        binding.oilServiceCar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                populateOilServices();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                binding.oilServiceCar.setSelection(0);
            }
        });

        mAuth = FirebaseAuth.getInstance();
        binding.oilServiceSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                common.signOutPressed(mAuth, getActivity());
            }
        });

        return view;
    }

    private void populateCars() {
        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        carReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("Cars");

        carReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // CLEAR SO ITEMS DON'T DUPLICATE
                carList.clear();

                for (DataSnapshot item : snapshot.getChildren()) {
                    carList.add(item.child("carBrand").getValue().toString() + " "
                            + item.child("carModel").getValue().toString() + " "
                            + item.child("carYear").getValue().toString());
                }
                carAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }

    private void populateOilServices() {
        mAuth = FirebaseAuth.getInstance();
        String username = common.usernameFromEmail(mAuth.getCurrentUser().getEmail());
        mReference = FirebaseDatabase.getInstance().getReference().child("Customer")
                .child(username).child("oilService");

        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // CLEARING HERE SO DATA WONT DUPLICATE
                oilServiceList.clear();

                for (DataSnapshot item : snapshot.getChildren()) {
                    OilService oilService = item.getValue(OilService.class);

                    // ONLY SHOW SERVICES FOR CURRENTLY SELECTED CAR
                    if (oilService.getServiceCar().equals(binding.oilServiceCar
                            .getSelectedItem().toString())) {
                        oilServiceList.add(oilService);

                        // GET DATE POSTED FROM DB
                        LocalDate datePosted = LocalDate.parse(item.child("servicePosted")
                                .getValue().toString());
                        // ADD A YEAR TO POSTED DATE AS OIL SERVICE
                        // SHOULD BE DONE EVERY YEAR
                        LocalDate dateFuture = datePosted.plusYears(1);
                        LocalDate now = LocalDate.now();
                        // GET DAYS BETWEEN CURRENT DATE AND DATE WHEN
                        // NEXT OIL SERVICE SHOULD BE DONE
                        long noOfDaysBetween = DAYS.between(now, dateFuture);
                        // TODO: 12/11/2020 cut off here
                        String result = String.valueOf(noOfDaysBetween);

                        if (result.charAt(result.length() - 1) == '1')
                            binding.oilServiceRemainingTime.setText(getString
                                    (R.string.remaining_time_oil_one, result));
                        else
                            binding.oilServiceRemainingTime.setText(getString
                                    (R.string.remaining_time_oil, result));
                    }

                    if (oilServiceList.isEmpty()) {
                        binding.oilServiceImage.setVisibility(View.VISIBLE);
                        binding.oilServiceRecyclerView.setVisibility(View.GONE);
                        binding.oilServiceDivider.setVisibility(View.GONE);
                        binding.oilServiceCard.setVisibility(View.GONE);
                    } else {
                        binding.oilServiceImage.setVisibility(View.GONE);
                        binding.oilServiceRecyclerView.setVisibility(View.VISIBLE);
                        binding.oilServiceDivider.setVisibility(View.VISIBLE);
                        binding.oilServiceCard.setVisibility(View.VISIBLE);
                    }

                    oilServiceAdapter = new OilServiceAdapter(getActivity(), oilServiceList);
                    binding.oilServiceRecyclerView.setAdapter(oilServiceAdapter);
                }
                if (oilServiceList.size() > 0) {
                    Collections.reverse(oilServiceList);
                    oilServiceAdapter.notifyDataSetChanged();

                    oilServiceAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            Intent intent = new Intent(getActivity(), OilServiceDetails.class);
                            OilService clickedOilService = oilServiceList.get(position);
                            intent.putExtra(EXTRA_DATE_OIL, clickedOilService.getServiceDate());
                            intent.putExtra(EXTRA_CAR_OIL, clickedOilService.getServiceCar());
                            intent.putExtra(EXTRA_DESC_OIL, clickedOilService.getServiceDescription());
                            intent.putExtra(EXTRA_KM_OIL, clickedOilService.getServiceKM());
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                common.displaySnackbar(snackbarView, error.getMessage(), colorError, colorWhite);
            }
        });
    }
}
