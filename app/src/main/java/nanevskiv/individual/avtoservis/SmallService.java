package nanevskiv.individual.avtoservis;

public class SmallService extends MainService {

    public SmallService() {
    }

    public SmallService(String serviceCar, String serviceKM, String serviceDescription,
                        String serviceDate) {
        super(serviceCar, serviceKM, serviceDescription, serviceDate);
    }
}
